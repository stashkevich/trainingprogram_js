'use strict';

/*
* Расширения для модуля "Календарь событий"
* Позволяет задать функцию, которая будет вызываться (либо для заданного события, либо для всех событий сразу) за
* установленное количество времени до наступления основного события.
* */
var calendarEvents = (function (calendarEvents) {

    function getDateForPreEvent(eventDate, time) {
        var preEventTimeArray = time.split(':');
        var preEventDate = new Date(eventDate);

        preEventDate.setHours(preEventDate.getHours() - (+preEventTimeArray[0]));
        preEventDate.setMinutes(preEventDate.getMinutes() - (+preEventTimeArray[1]));
        preEventDate.setSeconds(preEventDate.getSeconds() - (+preEventTimeArray[2]));

        if(isNaN(Date.parse(preEventDate))) {
            return false;
        }
        return preEventDate;
    }

		calendarEvents.setPreFunctionForEvent = function (id, callbackFunction, time) {

        if (!id) {
            return console.log('Вы не ввели идентификатор события !');
        }

        if (!time) {
            return console.log('Вы не ввели время !');
        }

        var callback = callbackFunction;

        if(!callback) {
            var moduleConfig = calendarEvents.getModuleConfig();
            callback = moduleConfig.callFunction;
        }

        var parentEvent = calendarEvents.getEvents().byId(id);

				if (parentEvent) {
            var preEventDate = getDateForPreEvent(parentEvent.event.date, time);

            if(!preEventDate) {
                return console.log('Вы ввели не корректное время !');
            }

						var preEvent = calendarEvents.createEvent('preEvent', preEventDate, callback);

						preEvent.parent = parentEvent.event.id;
						preEvent.preEventType = 'local';
						preEvent.parentTimeDifference  = time;

						calendarEvents.addEventToQueue(preEvent);

						if (parentEvent.preFunction === undefined) {
								var preFunctionConfig = {
										preFunction: {
												value: true,
												writable: true,
												configurable: true,
												enumerable: true
										}
								};
								calendarEvents.setEventsProperty(id, preFunctionConfig);
						}
				}
		};

		calendarEvents.setPreFunctionForAllEvents = function (callbackFunction, time) {

        if (!time) {
            return console.log('Вы не ввели время !');
        }

        var callback = callbackFunction;

				if (!callback) {
						var moduleConfig = calendarEvents.getModuleConfig();
						callback = moduleConfig.callFunction;
				}

        var checkPreEventTime = getDateForPreEvent(new Date(), time);

        if (!checkPreEventTime) {
            return console.log('Вы ввели не корректное время !');
        }

				var preEventModuleConfigProperty = {
						preFunction: {
								value: {
										callbackFunction: callback,
										time: time
								},
								writable: true,
								configurable: true,
								enumerable: true
						}
				};

				calendarEvents.setConfigProperties(preEventModuleConfigProperty);

				var currentEvents = (calendarEvents.getEvents().all()).slice();

				for (var i = 0; i < currentEvents.length; i++) {
						if(currentEvents[i].preEventType !== 'global') continue;
            calendarEvents.deleteEvent(currentEvents[i].id);
				}

				currentEvents = (calendarEvents.getEvents().all()).slice();

				if (currentEvents.length > 0) {

						var preEventDate;

						for (var j = 0; j < currentEvents.length; j++) {
								if (currentEvents[j].parent === undefined) {

										preEventDate = getDateForPreEvent(currentEvents[j].date, time);

										var preEvent = calendarEvents.createEvent('preEvent', preEventDate, callbackFunction);

										preEvent.parent = currentEvents[j].id;
										preEvent.preEventType = 'global';
										preEvent.parentTimeDifference  = time;

										calendarEvents.addEventToQueue(preEvent);

										if (currentEvents[j].preFunction === undefined){
												var preFunctionConfig = {
														'preFunction': {
																'value': true,
																'writable': true,
																'configurable': true,
																'enumerable': true
														}
												};

												calendarEvents.setEventsProperty(currentEvents[j].id, preFunctionConfig);
										}
								}
						}
				}

		};

		calendarEvents.addSubscriber('EVENT_ADDED_TO_QUEUE', function(event) {

				alert('EVENT_ADDED_TO_QUEUE');
				if (event.parent === undefined) {

						var moduleConfig = calendarEvents.getModuleConfig();
						if (moduleConfig.preFunction !== undefined) {

								var preEventDate = getDateForPreEvent(event.date, moduleConfig.preFunction.time);

								var preEvent = calendarEvents.createEvent('preEvent', preEventDate, event.callFunction);

								preEvent.parent = event.id;
								preEvent.preEventType = 'global';
								preEvent.parentTimeDifference  = moduleConfig.preFunction.time;

								calendarEvents.addEventToQueue(preEvent);

								var preFunctionConfig = {
										'preFunction': {
												'value': true,
												'writable': true,
												'configurable': true,
												'enumerable': true
										}
								};

								calendarEvents.setEventsProperty(event.id, preFunctionConfig);
						}
				}
		});

		calendarEvents.addSubscriber('EVENT_IS_SHOWN', function(event) {
				alert('PRE EVENT_IS_SHOWN');
				if (event.isRecurring !== undefined) {
					var allEvents = (calendarEvents.getEvents().all()).slice();
					for (var i = 0; i < allEvents.length; i++) {
						if (allEvents[i].parent === event.id) {
							var preEventDate = getDateForPreEvent(event.date, allEvents[i].parentTimeDifference);
							calendarEvents.updateEvent(allEvents[i].id, '', preEventDate);
						}
					}
				}
		});

		calendarEvents.addSubscriber('EVENT_REMOVED', function(event) {

				alert('EVENT_REMOVED');

				if (event.parent === undefined) {
						var allEvents = (calendarEvents.getEvents().all()).slice();
						for (var i = 0; i < allEvents.length; i++) {
								if (allEvents[i].parent === event.id) {
										calendarEvents.deleteEvent(allEvents[i].id)
								}
						}
				}
		});

		calendarEvents.addSubscriber('EVENT_UPDATE', function(event) {

				alert('EVENT_UPDATE');

				if(event.preFunction !== undefined) {
						var allEvents = (calendarEvents.getEvents().all()).slice();
						for (var i = 0; i < allEvents.length; i++) {
								if (allEvents[i].parent === event.id) {
										var preEventDate = getDateForPreEvent(event.date, allEvents[i].parentTimeDifference);
										calendarEvents.updateEvent(allEvents[i].id, '', preEventDate);
								}
						}
				}

		});

		return calendarEvents;

})(calendarEvents || {});